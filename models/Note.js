const {Schema, model} = require('mongoose');

const schema = new Schema(
    {
      userId: {
        type: String,
        required: true,
      },
      completed: {
        type: Boolean,
        default: false,
      },
      text: {
        type: String,
        required: true,
      },
      createdDate: {
        type: Date,
        required: true,
      },
    },
    {versionKey: false},
);

module.exports = model('Note', schema);
