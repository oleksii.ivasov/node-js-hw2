const jwt = require('jsonwebtoken');
require('dotenv').config();

module.exports = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(' ')[1];
    const decoded = jwt.verify(token, process.env.secret);
    req.userData = decoded;
    next();
  } catch (e) {
    return res.status(400).json({message: 'Auth failed'});
  }
};
