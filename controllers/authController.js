const User = require('../models/User');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
require('dotenv').config();
const {validationResult} = require('express-validator');
const secret = process.env.secret;
const generateAccessToken = (id) => {
  const payload = {id};
  return jwt.sign(payload, secret, {expiresIn: '24h'});
};

class AuthController {
  async registration(req, res) {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res
            .status(400)
            .json({message: 'Error while register', errors});
      }
      const {username, password} = req.body;
      const candidate = await User.findOne({username});
      if (candidate) {
        return res
            .status(400)
            .json({message: 'User with this username already exists'});
      }
      const hashPassword = bcrypt.hashSync(password, 7);
      const createdDate = new Date();
      const user = new User({username, password: hashPassword, createdDate});
      await user.save();
      return res.json({message: 'Success'});
    } catch (e) {
      console.log(e);
      res.status(400).json({message: 'Registation error'});
    }
  }

  async login(req, res) {
    try {
      const {username, password} = req.body;
      const user = await User.findOne({username});
      if (!user) {
        return res
            .status(400)
            .json({message: `User ${username} isn't found`});
      }
      const validPassword = bcrypt.compareSync(password, user.password);
      if (!validPassword) {
        return res.status(400).json({message: 'Wrong password'});
      }
      const token = generateAccessToken(user._id);
      return res.json({message: 'Success', jwt_token: token});
    } catch (e) {
      console.log(e);
      res.status(400).json({message: 'Login error'});
    }
  }
}

module.exports = new AuthController();
