const bcrypt = require('bcryptjs/dist/bcrypt');
const User = require('../models/User');
const {validationResult} = require('express-validator');

class UsersController {
  async getUser(req, res) {
    try {
      const user = await User.findOne().sort({_id: -1});
      if (!user) {
        return res.status(400).json({message: `There is no users yet`});
      }
      user.password = undefined;
      return res.json({user: user});
    } catch (e) {
      console.log(e);
      res
          .status(400)
          .json({message: 'Error happened while trying to get user'});
    }
  }
  async deleteUser(req, res) {
    try {
      const user = await User.findOne().sort({_id: -1});
      if (!user) {
        return res.status(400).json({message: `There is no users yet`});
      }
      await User.deleteOne({_id: user._id});
      return res.json({message: 'Success'});
    } catch (e) {
      console.log(e);
      res.status(400).json({
        message: 'Error happened while trying to delete user profile',
      });
    }
  }
  async changePassword(req, res) {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res
            .status(400)
            .json({message: 'Error while register', errors});
      }
      const {oldPassword, newPassword} = req.body;
      const user = await User.findOne().sort({_id: -1});
      if (!user) {
        return res.status(400).json({message: `There is no users yet`});
      }
      const validPassword = await bcrypt.compare(oldPassword, user.password);
      if (!validPassword) {
        return res.status(400).json({message: `Wrong password`});
      }
      const hashPassword = bcrypt.hashSync(newPassword, 7);
      await User.findOneAndUpdate(
          {_id: user._id},
          {password: hashPassword},
      );
      return res.json({message: `Success`});
    } catch (e) {
      console.log(e);
      res
          .status(400)
          .json({message: `Error happened while trying to change password`});
    }
  }
}

module.exports = new UsersController();
