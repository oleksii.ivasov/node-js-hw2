const Note = require('../models/Note');
const User = require('../models/User');

class NoteController {
  async renderNotes(req, res) {
    try {
      const notes = await Note.find({}).lean();
      res.render('index', {
        title: 'Notes list',
        isIndex: true,
        notes,
      });
    } catch (e) {
      console.log(e);
      res.status(400).json({message: 'Render error'});
    }
  }

  async getNotes(req, res) {
    try {
      const {offset, limit} = req.query;
      const user = await User.findOne().sort({_id: -1});
      const userId = user.id;
      const notes = await Note.find({userId}).skip(offset).limit(limit);
      res.json({offset, limit, count: 0, notes});
    } catch (e) {
      console.log(e);
      res.status(400).json({message: 'Get notes error'});
    }
  }

  async getNoteById(req, res) {
    try {
      const noteId = req.params.id;
      const note = await Note.findOne({_id: noteId});
      res.json({note});
    } catch (e) {
      console.log(e);
      res.status(400).json({message: 'Get note error'});
    }
  }

  async updateNoteById(req, res) {
    try {
      const newText = req.body.text;
      const noteId = req.params.id;
      const note = await Note.findOne({_id: noteId});
      note.text = newText;
      await note.save();
      res.json({message: 'Success'});
    } catch (e) {
      console.log(e);
      res.status(400).json({message: 'Update note error'});
    }
  }

  async changeNoteStatus(req, res) {
    try {
      const noteId = req.params.id;
      const note = await Note.findOne({_id: noteId});
      note.completed = !note.completed;
      await note.save();
      res.json({message: 'Success'});
    } catch (e) {
      console.log(e);
      res.status(400).json({message: 'Change note status error'});
    }
  }
  async deleteNoteById(req, res) {
    try {
      const noteId = req.params.id;
      await Note.findOneAndDelete({_id: noteId});
      res.json({message: 'Success'});
    } catch (e) {
      console.log(e);
      res.status(400).json({message: 'Delete note error'});
    }
  }

  async renderCreatePage(req, res) {
    try {
      res.render('create', {
        title: 'Create note',
        isCreate: true,
      });
    } catch (e) {
      console.log(e);
      res.status(400).json({message: 'Render error'});
    }
  }

  async createNote(req, res) {
    try {
      const user = await User.findOne().sort({_id: -1});
      const note = new Note({
        text: req.body.text,
        createdDate: new Date(),
        userId: user._id,
      });
      await note.save();
      res.json({message: 'Success'});
      // res.redirect('/');
    } catch (e) {
      console.log(e);
      res.status(400).json({message: 'Create note error'});
    }
  }

  async renderNoteStatus(req, res) {
    try {
      const note = await Note.findById(req.body.id);
      note.completed = !!req.body.completed;
      await note.save();
      res.redirect('/');
    } catch (e) {
      console.log(e);
      res.status(400).json({message: 'Render error'});
    }
  }
}

module.exports = new NoteController();
