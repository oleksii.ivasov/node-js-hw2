const {Router} = require('express');

const router = Router();
const controller = require('../controllers/authController');
const {check} = require('express-validator');

router.post(
    '/register',
    [
      check('username', 'Username cannot be empty').notEmpty(),
      check(
          'password',
          'Password must be more than 4 and less than 10 characters',
      ).isLength({min: 4, max: 10}),
    ],
    controller.registration,
);
router.post('/login', controller.login);

module.exports = router;
