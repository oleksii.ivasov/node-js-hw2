const {Router} = require('express');
const controller = require('../controllers/usersController');
const {check} = require('express-validator');
const checkAuth = require('../middleware/check-auth');

const router = Router();

router.get('/me', checkAuth, controller.getUser);

router.delete('/me', checkAuth, controller.deleteUser);

router.patch(
    '/me',
    [
      check(
          'oldPassword',
          'Password must be more than 4 and less than 10 characters',
      ).isLength({
        min: 4,
        max: 10,
      }),
      check(
          'newPassword',
          'Password must be more than 4 and less than 10 characters',
      ).isLength({
        min: 4,
        max: 10,
      }),
    ],
    checkAuth,
    controller.changePassword,
);

module.exports = router;
