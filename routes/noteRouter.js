const {Router} = require('express');
const controller = require('../controllers/noteContoller');
const checkAuth = require('../middleware/check-auth');

const router = Router();

router.get('/', controller.renderNotes);

router.get('/api/notes', checkAuth, controller.getNotes);

router.get(`/api/notes/:id`, checkAuth, controller.getNoteById);

router.put(`/api/notes/:id`, checkAuth, controller.updateNoteById);

router.patch(`/api/notes/:id`, checkAuth, controller.changeNoteStatus);

router.delete(`/api/notes/:id`, checkAuth, controller.deleteNoteById);

router.get('/api/create', controller.renderCreatePage);

router.post('/api/notes', controller.createNote);

router.post('/api/complete', controller.renderNoteStatus);

module.exports = router;
