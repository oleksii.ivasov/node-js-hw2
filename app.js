const express = require('express');
const mongoose = require('mongoose');
require('dotenv').config();
const exphbs = require('express-handlebars');
const path = require('path');
const cors = require('cors');
const noteRouter = require('./routes/noteRouter');
const authRouter = require('./routes/authRouter');
const usersRouter = require('./routes/usersRouter');

const app = express();
const hbs = exphbs.create({
  defaultLayout: 'main',
  extname: 'hbs',
});
app.engine('hbs', hbs.engine);
app.set('view engine', 'hbs');
app.set('views', 'views');
app.use(express.urlencoded({extended: true}));
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.json());
app.use(cors());
app.use('/api/auth', authRouter);
app.use('/api/users', usersRouter);
app.use(noteRouter);
const PORT = process.env.PORT || 8080;
const MONGO_ATLAS = process.env.MONGO_ATLAS;


async function start() {
  try {
    await mongoose.connect(
        MONGO_ATLAS,
        {
          useNewUrlParser: true,
        // useFindAndModify: false,
        },
    );
    app.listen(PORT, () => {
      console.log(`Server is working on port ${PORT}`);
    });
  } catch (e) {
    console.log(e);
  }
}

start();
